from setuptools import setup

setup(
        name='pyed_gallery_db', 
        version='0.1.0', 
        description='A toy gallery',
        url='https://bitbucket.org/mihand/pyed_gallery_db',
        license='MIT',
        packages=['pyed_gallery_db'],
        install_requires=[
           'Flask==0.12.2',
           'Jinja2==2.9.6'
        ], 
        extras_require={
            'test': ['pytest'],
        },
        #package_data={}
        #entry_points={}
)

